const body = document.body;
let bodyStyle = body.classList;
const mode = localStorage.getItem("mode");
let logo = document.querySelector("#logo").classList;
window.onload = function () {
  if (mode !== "") {
    bodyStyle.toggle(mode);
  }
};

const btn = document.querySelector("#btn");
btn.onclick = () => {
  const mode = localStorage.getItem("mode");
  if (mode === "") {
    localStorage.setItem("mode", "black");
    bodyStyle.add("black");
    logo.add("black");
  } else {
    localStorage.setItem("mode", "");
    bodyStyle.remove("black");
    logo.remove("black");
  }
};
