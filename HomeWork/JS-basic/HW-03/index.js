let firstNum;
let secondNum;
let operWithNum;

do {
  firstNum = +prompt("Enter first number", 6);
} while (checkNum(firstNum));
do {
  secondNum = +prompt("Enter second number", 3);
} while (checkNum(secondNum));
do {
  operWithNum = prompt("Enter operation with numbers", "*");
} while (!isNaN(operWithNum));

function checkNum(number) {
  return !number || Number.isNaN(+number);
}

function performMath(firstNum, secondNum, operWithNum) {
  if (operWithNum === "+") {
    return firstNum + secondNum;
  } else if (operWithNum === "-") {
    return firstNum - secondNum;
  } else if (operWithNum === "*") {
    return firstNum * secondNum;
  } else if (operWithNum === "/") {
    return firstNum / secondNum;
  }
}

console.log(performMath(firstNum, secondNum, operWithNum));
