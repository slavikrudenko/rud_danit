$(function () {
  $(".tabs-title").click(function () {
    $(".tabs").each(function () {
      $(".tabs li ").removeClass("active");
    });
    const id = $(this).attr("id");
    $(`#${id}`).addClass("active");
    text(id);
  });

  function text(id) {
    $(".tabs-content li").each(function () {
      if (id === $(this).attr("class")) {
        $(this).addClass("active-text");
      } else {
        $(this).removeClass("active-text");
      }
    });
  }
});
