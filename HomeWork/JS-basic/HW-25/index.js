const img = document.querySelectorAll(".image-to-show");

let i = 0;
let index = 1;

const nextBtn = document.querySelector("#next");
nextBtn.onclick = () => {
  onClickImg();
  sliderAnimation(-600);

  i++;
};

const previousBtn = document.querySelector("#previous");
previousBtn.onclick = () => {
  onClickImg();
  sliderAnimation(600);
  i--;
};

function onClickImg() {
  if (i === img.length) {
    i = 0;
  } else if (i < 0) {
    i = img.length - 1;
  }
  img[i].style.zIndex = index++;
}

function sliderAnimation(length) {
  img[i].animate(
    [{ transform: `translateX(${length}px)` }, { transform: "translateX(0)" }],
    {
      duration: 1000,
    }
  );
  console.log(i);
}
