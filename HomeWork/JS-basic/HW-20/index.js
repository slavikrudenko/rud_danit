let vehicles = [
  ("locales",
  "contentType",
  ["Borispol", ["en_US", "Toyota"]],
  "description",
  "name",
  "en_US"),
];
function filterCollection(arr, str, boolean, ...arrStr) {
  const newStrArr = str.split(" ");
  let checkStr;

  if (boolean) {
    checkStr = arr.every(
      (n, i) => n.toLowerCase().trim() === newStrArr[i].toLowerCase().trim()
    );
  } else {
    checkStr = arr.every(
      (n, i) => n.toLowerCase().trim() === newStrArr[i].toLowerCase().trim()
    );
  }
  let newUserArr = [];
  newUserArr.push(...arrStr);
  console.log(newUserArr);

  if (checkStr) {
    let users = arr.filter((item) => {
      console.log(item);
      newUserArr.forEach((e) => {
        return console.log(item === e);
      });
    });
    console.log(users);
  }
}

filterCollection(
  vehicles,
  "en_US Toyota",
  true,
  "name",
  "description",
  "contentType",
  "locales",
  "locales"
);
