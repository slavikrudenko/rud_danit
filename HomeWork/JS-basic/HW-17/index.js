const userName = prompt("Enter your name");
const userSurname = prompt("Enter your last name");
let userLesons;
let userMark;

const student = {
  name: userName,
  lastName: userSurname,
  table: {},
};

while (true) {
  userLesons = prompt("Enter lessons");
  if (userLesons !== null) {
    userMark = prompt("Enter your mark");
  } else {
    break;
  }
  student.table[userLesons] = userMark;
}
console.log(student);

let numBadNum = 0;
for (let key in student.table) {
  if (student.table[key] < 4) {
    numBadNum++;
  }
}

if (numBadNum === 0) {
  console.log("Студент переведен на следующий курс");
}

let lengthMark = 0;
let summMark = 0;
let averageMarks;
for (let key in student.table) {
  lengthMark++;
  summMark += +student.table[key];
  averageMarks = summMark / lengthMark;
}
if (averageMarks > 7) {
  console.log("Студенту назначена стипендия");
}
