let n;
do {
  n = +prompt("Enter number", 10);
} while (!n || Number.isNaN(n));

function factRecursion(n) {
  let sum;
  if (n != 1) {
    sum = n * factRecursion(n - 1);
  } else {
    sum = 1;
  }
  return sum;
}
alert(`result: ${factRecursion(n)}`);
