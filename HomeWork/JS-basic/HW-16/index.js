let n;
let f1;
let f2;

do {
  f1 = +prompt("Enter Number F1");
} while (!Number.isInteger(f1));
do {
  f2 = +prompt("Enter Number F2");
} while (!Number.isInteger(f2));
do {
  n = +prompt("Enter Number");
} while (!Number.isInteger(n));

function fib(f1, f2, n) {
  let sum;
  if (n > 2) {
    for (let i = 2; i < n; i++) {
      sum = f1 + f2;
      f1 = f2;
      f2 = sum;
    }
  } else if (n < 0) {
    for (let i = 2; i < n; i++) {
      sum = f1 - f2;
      f1 = f2;
      f2 = sum;
    }
  } else if (n === 2) {
    return f2;
  } else if (n === 1) {
    return f1;
  } else {
    return n;
  }
  return sum;
}

console.log(fib(f1, f2, n));
