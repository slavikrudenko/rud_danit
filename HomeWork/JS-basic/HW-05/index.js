function createNewUser(firstName, lastName, birthday) {
  return {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    getLogin() {
      return `login: ${
        this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
      }`;
    },
    getAge() {
      return `age: ${Math.floor(
        (new Date().getTime() -
          new Date(
            birthday.substring(6),
            birthday.substring(3, 5) - 1,
            birthday.substring(0, 2)
          ).getTime()) /
          31557600000
      )}`;
    },
    getPassword() {
      return `password: ${
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        birthday.substring(6)
      }`;
    },
  };
}
const newUser = new createNewUser(
  prompt("Enter your name"),
  prompt("Enter your surname"),
  prompt("Enter your birthday, like: <dd.mm.yyyy>")
);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
console.log(newUser);
