function createTable() {
  const body = document.body;
  body.insertAdjacentHTML("afterbegin", `<table></table>`);
  const table = document.querySelector("table");
  table.insertAdjacentHTML("afterbegin", `<tbody></tbody>`);
  const tBody = document.querySelector("tbody");
  createColumn(tBody);
  tableClick(body, table);
  bodyClick(body, table);
}
createTable();

function createColumn(tBody) {
  for (let i = 0; i < 30; i++) {
    tBody.insertAdjacentHTML("beforeend", `<tr class = "column"></tr>`);
    let tColumn = document.querySelectorAll("tr")[i];
    createRow(tColumn);
  }
}

function createRow(tColumn) {
  for (let i = 0; i < 30; i++) {
    tColumn.insertAdjacentHTML("beforeend", `<td class = "row"></tr>`);
    const row = document.querySelectorAll(".row")[i];
  }
}

function tableClick(body, table) {
  table.addEventListener("click", (e) => {
    e.target.classList.value !== "black"
      ? e.target.classList.add("black")
      : e.target.classList.remove("black");
    e.stopPropagation();
  });
}

function bodyClick(body, table) {
  body.addEventListener("click", (e) => {
    table.classList.value !== "body"
      ? table.classList.add("body")
      : table.classList.remove("body");
  });
}
