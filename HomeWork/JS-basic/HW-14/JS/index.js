let scrollTime = 1000;
$(function () {
  $(".menu-link").on("click", "a", function (e) {
    e.preventDefault();
    let id = $(this).attr("href");
    let position = $(id).offset().top;
    $("body,html").animate({ scrollTop: position }, scrollTime);
  });

  $(".menu-link a:not(.menu-link-btn-first)").click(function (e) {
    $("body").append(
      $("<button>", {
        id: "btn-up",
        text: "up",
      })
    );
    $("#btn-up").click(function () {
      $("body,html").animate({ scrollTop: 0 }, scrollTime);
      $("#btn-up").remove();
    });
  });
  $("#hide").click(function () {
    $(".section-news").slideToggle();
  });
});
