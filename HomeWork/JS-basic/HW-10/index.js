//Password #1 click
const iconPassFirst = document.querySelector("#img-pass1");
const iconPassLast = document.querySelector("#img-pass2");
const inputPassFirst = document.querySelector("#input-pass1");
const inputPassLast = document.querySelector("#input-pass2");

function showPasswor(img, input) {
  img.addEventListener("mousedown", (e) => {
    if (input.type === "password") {
      img.classList.remove("fa-eye");
      img.classList.add("fa-eye-slash");
      input.setAttribute("type", "text");
    } else {
      img.classList.remove("fa-eye-slash");
      img.classList.add("fa-eye");
      input.setAttribute("type", "password");
    }
  });
}

showPasswor(iconPassFirst, inputPassFirst);
showPasswor(iconPassLast, inputPassLast);

//Submit - btn
const submitBtn = document.querySelector(".btn");
const errorPassword = document.querySelector(".error-password");

submitBtn.addEventListener("click", (e) => {
  if (inputPassFirst.value === inputPassLast.value) {
    errorPassword.innerHTML = "";
    alert("You are welcome");
  } else {
    errorPassword.innerHTML = "Нужно ввести одинаковые значения";
    errorPassword.style.color = "red";
  }
});
