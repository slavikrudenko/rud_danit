let img = document.querySelectorAll(".images-wrapper img");
let index = 1;
let i = 0;
let timer = 3000;
let timeout = 500;

function imgSlider() {
  const imgAlbom = setInterval(() => {
    if (i < img.length) {
      img[i].style.zIndex = index++;
      i++;
    } else {
      i = 0;
      img[i].style.zIndex = index++;
      i++;
    }
  }, timer);
  imgSlideStop(imgAlbom);
  imgSliderGo();
}
imgSlider();

function imgTimer() {
  let current = 1;
  timerId = setInterval(function () {
    const timer = document.querySelector("span");
    timer.innerHTML = `timer: ${current}`;
    current--;
    if (current < 0) {
      current = 2;
    }
  }, 1000);
}
imgTimer();

function imgSlideStop(imgAlbom) {
  let stopBtn = document.querySelector("#stop");
  stopBtn.onclick = () => {
    clearInterval(imgAlbom);
    clearInterval(timerId);
  };
}

function imgSliderGo() {
  let goBtn = document.querySelector("#go");
  goBtn.onclick = () => {
    imgSlider();
    imgTimer();
  };
}
