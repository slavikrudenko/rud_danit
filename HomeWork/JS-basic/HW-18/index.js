const obj = {
  name: "Германия",
  language: "немецкий",
  capital: {
    name: "Берлин",
    population: 3375000,
    year: 1237,
  },
};
const cloneObj = (obj) => {
  if (typeof obj === "object" && obj !== null) {
    const newObj = {};
    for (let key in obj) {
      newObj[key] = cloneObj(obj[key]);
    }
    return newObj;
  } else {
    return obj;
  }
};

console.log(cloneObj(obj));
