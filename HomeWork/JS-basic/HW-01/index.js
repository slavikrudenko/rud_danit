let userName = null;
let userAge = null;

do {
  userName = prompt("Enter your name");
} while (userName == "");
do {
  userAge = +prompt("Enter your age");
} while (!Number.isInteger(userAge));
if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert(`Welcome, ${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else if (userAge >= 22) {
  alert(`Welcome, ${userName}`);
}
console.log(userName);
console.log(userAge);
