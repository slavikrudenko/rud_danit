//div && span && Btn
const userPriceSpan = document.createElement("span");
userPriceSpan.style.display = "block";
const userPriceError = document.createElement("div");
const priceBtn = document.createElement("button");

//Input
const inputCreate = document.createElement("input");
document.body.prepend(inputCreate);
const input = document.querySelector("input");
input.placeholder = "Price";
input.type = "number";
//input.style.cssText = `border: 1px solid black`;

//focus
input.addEventListener("focus", (e) => {
  input.setAttribute("class", "focus");
  // input.style.outline = "none";
  // input.style.border = "1px solid green";
  userPriceError.remove();
});

//blur
input.addEventListener("blur", (e) => {
  input.setAttribute("class", "blur");
  //input.style.borderColor = "black";
  userPriceFunc();
});

// userPrice
function userPriceFunc() {
  if (input.value <= 0) {
    document.body.append(userPriceError);
    userPriceError.innerText = "Please enter correct price";
    input.setAttribute("class", "input-not-value");
  } else {
    document.body.prepend(userPriceSpan);
    userPriceSpan.innerHTML = `Текущая цена: ${input.value}`;
    input.style.color = "green";
    priceBtnFun();
  }
}

function priceBtnFun() {
  document.body.append(priceBtn);
  priceBtn.innerText = "X";
  priceBtn.setAttribute("class", "btn-price");
  //priceBtn.style.cssText = `padding:0 2px`;
  priceBtn.addEventListener("click", (e) => {
    userPriceSpan.remove();
    priceBtn.remove();
    input.value = "";
  });
}
