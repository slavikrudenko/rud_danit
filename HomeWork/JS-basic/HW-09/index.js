let title = document.querySelectorAll(".tabs-title");
let text = document.querySelectorAll(".tabs-content > li");

function something(e) {
  for (let i = 0; i < title.length; i++) {
    title[i].classList.remove("active");
  }
  e.classList.toggle("active");
  activeText(e);
}

function activeText(e) {
  let textItem = document.querySelector(`.${e.innerText}`);
  if (e.innerText === textItem.className) {
    for (let i = 0; i < text.length; i++) {
      text[i].classList.remove("active-text");
    }
    textItem.setAttribute("class", "active-text");
  }
}
