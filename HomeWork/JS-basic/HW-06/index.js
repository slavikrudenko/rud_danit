const filterBy = (userArr, dataTypes) => {
  const newFilter = userArr.filter((el) => {
    return typeof el !== dataTypes;
  });
  return newFilter;
};

console.log(filterBy(["hello", "world", 23, "23", null], "string"));
