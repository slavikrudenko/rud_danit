const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["1", "2", "3", "sea", "user", 23];
const arr3 = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

const newList = document.querySelector("ul");
// //Map
function itemList() {
  let newArr = [];
  newArr = arr3.map((el) => `<li>${el}</li>`);
  newArr.forEach((el) => newList.insertAdjacentHTML("afterbegin", el));
}
//itemList();

function printNumbers(from, to) {
  let current = from;
  let timerId = setInterval(function () {
    const timer = document.querySelector("div");
    timer.innerHTML = `timer: ${current}`;
    if (current == to) {
      newList.remove();
      timer.remove();
    }
    current--;
    console.log(timerId);
  }, 1000);
}

printNumbers(5, 0);

//forEach + обработкa вложенных массивов
function itemList2(arrray, node) {
  node = arrray.map((el) => {
    if (typeof el === "object") {
      return `<li>${itemList2(el, newListItem)}</ul>`;
    } else {
      return `<li>${el}</li>`;
    }
  });

  newList.forEach((el) => node.insertAdjacentHTML("beforeend", el));
}

//itemList2(arr3, newList);

//Recursion
// create new ul
// call itemList with node=new ul
// insert new ul with li into general list
